#Script to create a list of all vm's on each vcenter server defined in $viserver and their vmtools status.
#The list then gets emailed to the $sendTo email address
#Authored by Dan Harrell
#Created on 6/21/2019

#vCenter Servers
$vc1 = "vcenter1"
$vc2 = "vcenter2"
$vc3 = "vcenter3"
$viserver = $vc1,$vc2,$vc3

Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false

#credentials and email details 
$username = "User"
$password = "C:\Users\User\Documents\Scripts\password.txt"
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, (Get-Content $password | ConvertTo-SecureString)

#Define your sendTo parameters
$sendTo = @(emaildisto.list.org)
$bcc = "DefineBCCSendTo@email.org"
$from = "DefineSendFrom@email.org"
$smtp = "smtp.mail.relay.com"

$priority = "Normal"

$today = Get-Date -Format "MM/dd/yyyy"



foreach ($vc in $viserver){
    Connect-VIServer $vc -credential $cred
    $PathToExport = "c:\temp\" + $vc+ "_inventory_report_name_ds_host.csv"
    Get-VM | Get-View | Select-Object @{N="VM Name";E={$_.Name}},@{Name="VMware Tools";E={$_.Guest.ToolsStatus}},@{Name="OS";E={$_.Guest.GuestFullName}},@{Name="PowerState";E={$_.Guest.GuestState}}| Export-Csv -path $PathToExport -NoTypeInformation -UseCulture
    $Subject =  "Status Report of VM Tools on $vc"
    $testPath = Test-Path $PathToExport
        If ($testPath -eq $True) {
            #Email Message Building
            # Table name
            $tableName = “Report”
 
                    # Create Table object
                    $table = New-Object system.Data.DataTable “$tableName”
 
                    # Define Columns
                    $col1 = New-Object system.Data.DataColumn "VM Name",([string])
                    $col2 = New-Object system.Data.DataColumn "VMware Tools",([string])
                    $col3 = New-Object system.Data.DataColumn "OS",([string])
                    $col4 = New-Object system.Data.DataColumn "PowerState",([string])
 
                    # Add the Columns
                    $table.columns.add($col1)
                    $table.columns.add($col2)
                    $table.columns.add($col3)
                    $table.columns.add($col4)

                    $Result = Import-Csv -Path $PathToExport #| ConvertTo-Html -Fragment#
                        foreach ($line in $Result){
                                # Create a row
                                $row = $table.NewRow()
  
                                # Enter data in the row
                                $row."VM Name" = ($line."VM Name")
                                $row."Vmware Tools" = ($line."VMware Tools")
                                $row."OS" = ($line."OS")
                                $row."PowerState" = ($line."PowerState")
                                
 
                                # Add the row to the table
                                $table.Rows.Add($row)
                                }
                            }

#table html style for email body
$Head = 
@"
<style>
body {
font-family: "Arial";
font-size: 8pt;
color: #4C607B;
}
th, td { 
border: 1px solid #000000;
border-collapse: collapse;
padding: 5px;
}
th {
font-size: 1.2em;
text-align: Center;
background-color: #d2d6da;
color: #000000;
}
td {
color: #000000;
}
.even { background-color: #ffffff; }
.odd { background-color: #bfbfbf; }
</style>
"@

 
# Creating body
[string]$body = [PSCustomObject]$table | Select-Object -Property "VM Name","VMware Tools", "OS", "PowerState"| Sort-Object -Property "VM Name"  | ConvertTo-HTML -head $Head -Body "<font color=`"Black`"><h4>VMTools Status Report for $vc for $today</h4></font>"
if ($null -ne $PathToExport) {
    Send-MailMessage -To $sendTo -Subject $subject -BodyAsHtml $body -SmtpServer $smtp -From $From -Priority $priority -Attachments $PathToExport
}

Disconnect-viserver * -confirm:$false
Remove-Item -path $pathToExport
}

