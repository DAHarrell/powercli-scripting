#Script to create a list of VM's no tags assigned to it and email the list to the $sendTo email address
#Authored by Dan Harrell
#Created on 6/21/2019

#vCenter Servers
$vc1 = "vcenter1"
$vc2 = "vcenter2"
$vc3 = "vcenter3"
$viserver = $vc1,$vc2,$vc3

Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false

#credentials and email details 
$username = "User"
$password = "C:\Users\User\Documents\Scripts\password.txt"
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, (Get-Content $password | ConvertTo-SecureString)

#Define your sendTo parameters
$sendTo = @(emaildisto.list.org)
$bcc = "DefineBCCSendTo@email.org"
$from = "DefineSendFrom@email.org"
$smtp = "smtp.mail.relay.com"

$priority = "Normal"

$today = Get-Date -Format "MM/dd/yyyy"

foreach ($vc in $viserver){
    Connect-VIServer $vc -credential $cred
    $subject =  "VMware Weekly Tag Report: $vc"
    $PathToExport = "c:\temp\" + $vc+ "_Tag_Report.csv"
    $tags =  Get-VM | Where-Object{(Get-TagAssignment $_) -eq $null} 
    $tags | Select-Object Name, PowerState | export-csv -Path $pathToExport -NoTypeInformation -UseCulture
    $testPath = Test-Path $PathToExport
        If ($testPath -eq $True) {
            if ($null -ne $tags){
                #$csv = Import-Csv -Path $PathToExport | ConvertTo-Html -Fragment

                #Email Message Building
                # Table name
                $tabName = “Report”
 
                    # Create Table object
                    $table = New-Object system.Data.DataTable “$tabName”
 
                    # Define Columns
                    $col1 = New-Object system.Data.DataColumn "Name",([string])
                    $col2 = New-Object system.Data.DataColumn "PowerState",([string])
 
                    # Add the Columns
                    $table.columns.add($col1)
                    $table.columns.add($col2)

                    $tagResult = Import-Csv -Path $PathToExport #| ConvertTo-Html -Fragment#
                        foreach ($line in $tagResult){
                                # Create a row
                                $row = $table.NewRow()
  
                                # Enter data in the row
                                $row."Name" = ($line.name)
                                $row."PowerState" = ($line.PowerState)
 
                                # Add the row to the table
                                $table.Rows.Add($row)
                                }
#table html style for email body
$Head = 
@"
<style>
body {
font-family: "Arial";
font-size: 8pt;
color: #4C607B;
}
th, td { 
border: 1px solid #000000;
border-collapse: collapse;
padding: 5px;
}
th {
font-size: 1.2em;
text-align: Center;
background-color: #d2d6da;
color: #000000;
}
td {
color: #000000;
}
.even { background-color: #ffffff; }
.odd { background-color: #bfbfbf; }
</style>
"@

# Creating body
[string]$body = [PSCustomObject]$table | Select-Object -Property "Name","PowerState" | Sort-Object -Property "Name"  | ConvertTo-HTML -head $Head -Body "<font color=`"Black`"><h4>No Tag Report for $vc for for the week of $today</h4></font>"

Send-MailMessage -To $sendTo -Subject $subject -BodyAsHtml $body -SmtpServer $smtp -From $From -Priority $priority

    Disconnect-viserver $vc -confirm:$false
    Remove-Item -path $pathToExport
}
}
}