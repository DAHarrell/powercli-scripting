#List of VM's, The cluster they are in, and which host it belongs to for the $dc and exports it to the users desktop
#Authored by Dan Harrell
#Created on 8/1/2019


#VARIABLE to define Datacenter name from vcenter
$dc = "virtual datacenter name"
#VARIABLE to define the vcenter server name to connect to
$vc = "vcenter1"

Connect-VIServer $vc

#Pulls inventory from the datacenter by VM, Cluster and its respective host
get-datacenter $dc | Get-VMhost | GET-VM | Select-Object  Name, @{N="Cluster";E={Get-Cluster -VM $_}}, @{N="ESX Host";E={Get-VMHost -VM $_}} | Export-Csv -path "c:\users\$env:USERNAME\desktop\vcenter_hosts.csv" -NoTypeInformation -UseCulture

Disconnect-viserver * -confirm:$false