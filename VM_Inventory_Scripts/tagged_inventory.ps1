#Collects information from each vCenter defined by $viserver to compile it into a table and csv attachment
#Once complete information is emails information to $sendTo email address and $bcc 
#This assumes that tags are created on your vm's with categories defined as owner, function, environment, application, and OS

#Authored by Dan Harrell - 
#Created on 8/1/2019

#vCenter Servers
$vc1 = "vcenter1"
$vc2 = "vcenter2"
$vc3 = "vcenter3"
$viserver = $vc1,$vc2,$vc3

Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false

#credentials and email details 
$username = "User"
$password = "C:\Users\User\Documents\Scripts\password.txt"
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, (Get-Content $password | ConvertTo-SecureString)

#Define your sendTo parameters
$sendTo = @(emaildisto.list.org)
$bcc = "DefineBCCSendTo@email.org"
$from = "DefineSendFrom@email.org"
$smtp = "smtp.mail.relay.com"

$priority = "Normal"

foreach ($vcenter in $viserver){
    Connect-VIServer $vcenter -credential $cred
        $subject =  "Tagged Inventory Report for $vcenter"
        $PathToExport = "c:\temp\" + $vcenter+ "_Inventory_Report.csv"

# Table name
$tabName = “Report”

#Create Table object
$table = New-Object system.Data.DataTable “$tabName”
                    # Define Columns
                    $col1 = New-Object system.Data.DataColumn "Name",([string])
                    $col2 = New-Object system.Data.DataColumn "OS",([string])
                    $col3 = New-Object system.Data.DataColumn "Application",([string])  
                    $col4 = New-Object system.Data.DataColumn "Environment",([string]) 
                    $col5 = New-Object system.Data.DataColumn "Owner",([string]) 
                    $col6 = New-Object system.Data.DataColumn "Function",([string]) 
                    # Add the Columns
                    $table.columns.add($col1)
                    $table.columns.add($col2)
                    $table.columns.add($col3)
                    $table.columns.add($col4)
                    $table.columns.add($col5)
                    $table.columns.add($col6)
#collect all relevant data
        $AllTheVMs = Get-VM 
        foreach ($vm in $AllTheVMs){
            $vmName = $vm | Select-Object-Object Name
            $Apptag = get-tagassignment -Category Application -entity $vm
            $Ostag = get-tagassignment -Category os -entity $vm
            $EnvTag = get-tagassignment -Category Environment -entity $vm
            $OwnerTag = get-tagassignment -Category Owner -entity $vm
            $FunctionTag = get-tagassignment -Category Function -entity $vm
         
         #Fill table with information
                    # Create a row
                    $row = $table.NewRow()
                    # Enter data in the row
                    $row."Name" = $vmName | Select-Object-Object -ExpandProperty Name -First 1 #| Select-Object -ExpandProperty Name -First 1
                    $row.os = $ostag | Select-Object -ExpandProperty tag -First 1 | Select-Object -ExpandProperty Name -First 1
                    $row.Application = $Apptag | Select-Object -ExpandProperty tag -First 1 | Select-Object -ExpandProperty Name -First 1
                    $row.Environment = $EnvTag | Select-Object -ExpandProperty tag -First 1 | Select-Object -ExpandProperty Name -First 1
                    $row.Owner = $OwnerTag | Select-Object -ExpandProperty tag -First 1 | Select-Object -ExpandProperty Name -First 1
                    $row.Function = $FunctionTag | Select-Object -ExpandProperty tag -First 1 | Select-Object -ExpandProperty Name -First 1
                    # Add the row to the table
                    $table.Rows.Add($row)        
        }
#table html style for email body
$Head = 
@"
<style>
body {
font-family: "Arial";
font-size: 8pt;
color: #4C607B;
}
th, td { 
border: 1px solid #000000;
border-collapse: collapse;
padding: 5px;
}
th {
font-size: 1.2em;
text-align: Center;
background-color: #d2d6da;
color: #000000;
}
td {
color: #000000;
}
.even { background-color: #ffffff; }
.odd { background-color: #bfbfbf; }
</style>
"@
 
# Creating body
[string]$body = [PSCustomObject]$table | Select-Object -Property "Name","OS", "Application", "Environment", "Owner", "Function" | Sort-Object -Property "Name"  | ConvertTo-HTML -head $Head -Body "<font color=`"Black`"><h4>Tagged Inventory Report for $vcenter </h4></font>"

#export-csv for attachments
$table | export-csv $PathToExport -UseCulture -NoTypeInformation

Disconnect-viserver $vcenter -confirm:$false

#Don't email if the script fails or no inventory is pulled 
if ($null -ne $PathToExport){
Send-MailMessage -To $sendTo -Bcc $bcc -Subject $subject -BodyAsHtml $body -SmtpServer $smtp -From $From -Attachments $PathToExport}
Remove-Item -path $pathToExport 
}