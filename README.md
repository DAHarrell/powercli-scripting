# PowerShell Scipting

# Repository for useful PowerCLI scripts 

This is a repository for useful scripts and commands that I have written and used in production environments to accomplish multiple
goals.


## Accessing the Repository
* Load the Git repository page: 
* Click on the “Clone or Download” button and then click “Download ZIP”
* Once downloaded, extract the zip file to the location of your choosing
* At this point, you now have a local copy of the repository

## Prerequisites

* You will need Powershell, PowerCLI to use these scripts and at minimum one ESXi host but they will also work on vCenter for larger environments

* PowerCLI can be installed by running Install-Module -Name VMware.PowerCLI inside your PowerShell Terminal.

* UCS PowerTool can be downloaded from <a href="https://community.cisco.com/t5/cisco-developed-ucs-integrations/cisco-ucs-powertool-suite-powershell-modules-for-cisco-ucs/ta-p/3639523"> UCS PowerTool Download</a>


## Built With

* My imagination and desire to learn.

## Authors

* **Dan Harrell** - *Initial work* - [Dan Harrell](https://www.linkedin.com/in/daharrell/)

## License

This project and code is licensed under the GNU GENERAL PUBLIC LICENSE - see the [LICENSE.md](LICENSE.md) file for details

## Disclaimer

THIS PROJECT AND IT'S RELATED CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


